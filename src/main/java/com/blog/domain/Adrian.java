package com.blog.domain;


import javax.persistence.*;

@Entity
@Table(name="adrian")
public class Adrian {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String adi;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAdi() {
        return adi;
    }

    public void setAdi(String adi) {
        this.adi = adi;
    }

    @Override
    public String toString() {
        return "Adrian{" +
            "id=" + id +
            ", adi='" + adi + '\'' +
            '}';
    }
}
