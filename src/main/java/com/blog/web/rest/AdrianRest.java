package com.blog.web.rest;

import com.blog.domain.Adrian;
import com.blog.repository.AdrianRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api")
public class AdrianRest {

    @Autowired
    AdrianRepository adrianRepository;

    @PostMapping("/test")
    public String testGet(@RequestBody Adrian adrian){
        System.out.println(adrian);
        if(adrian.getId()!=null)
            adrianRepository.save(adrian);
        else
            System.out.println("Param is empty");

        return "hei from get";
    }
}
