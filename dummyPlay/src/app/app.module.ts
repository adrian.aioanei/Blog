import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { TableComponent } from './table/table.component';
import { RouterModule } from '@angular/router';
import { FormComponent } from './form/form.component';
import { FormGroup, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    TableComponent,
    FormComponent
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    RouterModule.forRoot([
      {path:'table', component: TableComponent},
      {path:'form', component: FormComponent}
    ])
  ],

  bootstrap: [AppComponent]
})
export class AppModule { }
